<?php

/**
 * Plugin Name: 21128plugin
 * Description: Prosta cenzura postów.
 * Version: 1.0.0
 * Author:  Patryk Piosik
 * License: MIT
 */

/*
add_action("admin_notices", "notice");

    function notice(){
        ?>
        <div class="notice notice-success">
            <p>Cenzura postów działa!</p>
        </div>
        <?php
    }
   */

    add_filter("the_content","badWords");

    
    
        function badWords(){
            $content = get_the_content();
            $Replace = "***";
            $veryBadWords = [];
            if(get_option( 'badWords' ) == ''){
                $veryBadWords[] = 'politechnik';
            }else{
                $veryBadWords = get_option( 'badWords' );
            }
            
       
            for ($i = 0; $i < count($veryBadWords); $i++) {
            $content = str_ireplace($veryBadWords[$i],$Replace,$content);
            }
            return $content;
        }
        



function settings_page(){   
    echo '<h3>Ustawienia Cenzury</h3>';
    include_once('ustawienia_cenzury.php');
}


function wpdocs_register_my_custom_menu_page(){
    add_menu_page( 
        __( 'Opcje cenzury', 'textdomain' ),
        'Dodaj Cenzure',
        'manage_options',
        'dodajcenzure',
        'my_custom_menu_page',
        'none',
        6
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
 

function my_custom_menu_page(){

        echo '<h3>Ustawienia Cenzury</h3>';
        echo '<h4 class="notice notice-danger">Żeby usunąc liste - kliknij przycisk dodaj z pustym text inputem</h4>';
    include_once('ustawienia_cenzury.php');
}



